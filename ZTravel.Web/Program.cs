using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using ZTravel.API.DBF;

namespace ZTravel.Web {
	public class Program {
		public static Dictionary<string, DbfTypeMapping> TypeMapping =
			DbfTypeMapping.FromJson(new WebClient().DownloadString("https://raw.githubusercontent.com/derf/db-fakedisplay/master/share/ice_type.json"));
		public static void Main(string[] args) {
			CreateHostBuilder(args).Build().Run();
		}

		public static IHostBuilder CreateHostBuilder(string[] args) =>
			Host.CreateDefaultBuilder(args).ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });
	}
}