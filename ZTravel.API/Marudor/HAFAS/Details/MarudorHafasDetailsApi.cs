using System;
using System.Net;
using System.Web;

namespace ZTravel.API.Marudor.HAFAS.Details {
	public class MarudorHafasDetailsApi {
		public static MarudorHafasDetailsResponse GetDetails(string train, long epoch = 0) {
			var url = $"https://marudor.de/api/hafas/v1/details/{train}";
			Console.WriteLine(url);
			var raw = new WebClient().DownloadString(url);
			var json = MarudorHafasDetailsResponse.FromJson(raw);
			return json;
		}
	}
}