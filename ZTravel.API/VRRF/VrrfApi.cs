using System.Collections.Generic;
using System.Net;

namespace ZTravel.API.VRRF {
	public class VrrfApi {
		public static VrrfResponse GetDepartures(string api, string backend, string city, string station, int offset = 0, string line = "", string platform = "") {
			var url =
				$"https://{api}/{city}/{WebUtility.UrlEncode(station)}.json?backend={backend}&offset={offset}&line={line}&platform={platform}";
			var raw = new WebClient().DownloadString(url);
			var json = VrrfResponse.FromJson(raw);
			return json;
		}
	}
}