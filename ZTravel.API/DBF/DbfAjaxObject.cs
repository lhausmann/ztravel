using System;

namespace ZTravel.API.DBF {
	public class DbfAjaxObject {
		public bool HasDepartureTime;
		public bool HasArrivalTime;
		public DateTime DepartureTime;
		public DateTime ArrivalTime;
		public string TrainName;
		public string TrainClass;
		public string TrainSubtype;
		public string TrainNumber;
		public string Line;
		public string LineType;
		public string Origin;
		public string Destination;
		public string Route;
		public bool PlatformChanged = false;
		public string Platform;
		public int Delay;
		public string Info;
		public bool Cancelled;
		public bool Replacement;
	}
}