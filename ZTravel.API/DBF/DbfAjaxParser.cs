using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web;
using System.Xml.Linq;
// ReSharper disable PossibleNullReferenceException

namespace ZTravel.API.DBF {
	public class DbfAjaxParser {
		public static List<DbfAjaxObject> GetDepartures(string station, string via = "") {
			var url    = $"https://dbf.finalrewind.org/{HttpUtility.UrlEncode(station)}?via={via}&show_realtime=1&ajax=1";
			var result = new List<DbfAjaxObject>();
			var raw    = new WebClient().DownloadString(url);
			var xel    = XElement.Parse($"<root>{raw}</root>", LoadOptions.PreserveWhitespace);
			foreach (var el in xel.Elements()) {
				var connection = new DbfAjaxObject();
				if (!string.IsNullOrWhiteSpace(el.Attribute("data-line")?.Value))
					connection.Line = el.Attribute("data-line")?.Value.Trim();
				if (!string.IsNullOrWhiteSpace(el.Attribute("data-train")?.Value))
					connection.TrainName = el.Attribute("data-train")?.Value.Trim();
				if (!string.IsNullOrWhiteSpace(el.Attribute("data-from")?.Value))
					connection.Origin = el.Attribute("data-from")?.Value.Trim();
				if (!string.IsNullOrWhiteSpace(el.Attribute("data-to")?.Value))
					connection.Destination = el.Attribute("data-to")?.Value.Trim();
				if (!string.IsNullOrWhiteSpace(el.Attribute("data-platform")?.Value))
					connection.Platform = el.Attribute("data-platform")?.Value.Trim();
				if (!string.IsNullOrWhiteSpace(el.Attribute("data-no")?.Value))
					connection.TrainNumber = el.Attribute("data-no")?.Value.Trim();
				if (!string.IsNullOrWhiteSpace(el.Attribute("data-linetype")?.Value))
					connection.LineType = el.Attribute("data-linetype")?.Value.Trim();
				
				if (!string.IsNullOrWhiteSpace(el.Attribute("data-arrival")?.Value)) {
					connection.HasArrivalTime = true;
					connection.ArrivalTime    = DateTime.ParseExact(el.Attribute("data-arrival")?.Value.Trim(), "HH:mm", CultureInfo.InvariantCulture);
				}

				if (!string.IsNullOrWhiteSpace(el.Attribute("data-departure")?.Value)) {
					connection.HasDepartureTime = true;
					connection.DepartureTime    = DateTime.ParseExact(el.Attribute("data-departure")?.Value.Trim(), "HH:mm", CultureInfo.InvariantCulture);
				}

				var route = el.Descendants("span").FirstOrDefault(div => div.Attribute("class").Value.StartsWith("route"))?.Value;
				if (!string.IsNullOrWhiteSpace(route))
					connection.Route = route.Trim();
				
				var trainClass = el.Descendants("div").FirstOrDefault(div => div.Attribute("class").Value.StartsWith("line"))?.Value;
				if (!string.IsNullOrWhiteSpace(trainClass))
					connection.TrainClass = trainClass.Trim().Split("\n")[0].Trim();

				var trainSubtype = el.Descendants("div")
									 .FirstOrDefault(div => div.Descendants("span").Any(span => span.Attribute("class").Value.StartsWith("trainsubtype")))
									 ?.Descendants("span")
									 .FirstOrDefault(span => span.Attribute("class").Value.StartsWith("trainsubtype"))
									 ?.Value;
				if (!string.IsNullOrWhiteSpace(trainSubtype))
					connection.TrainSubtype = trainSubtype.Trim();
				
				var trainNumber = el.Descendants("div")
									 .FirstOrDefault(div => div.Descendants("span").Any(span => span.Attribute("class").Value.StartsWith("trainno")))
									 ?.Descendants("span")
									 .FirstOrDefault(span => span.Attribute("class").Value.StartsWith("trainno"))
									 ?.Value;
				if (!string.IsNullOrWhiteSpace(trainNumber))
					connection.TrainNumber = trainNumber.Trim();
				
				var platform = el.Descendants("span").FirstOrDefault(div => div.Attribute("class").Value.StartsWith("platform"));
				if (platform.Attribute("class")?.Value == "platform changed-platform") {
					connection.Platform        = platform.Value.Trim();
					connection.PlatformChanged = true;
				}

				var dest = el.Descendants("span").FirstOrDefault(div => div.Attribute("class").Value.StartsWith("dest"));
				if (dest.Attribute("class")?.Value == "dest  cancelled")
					connection.Cancelled = true;

				result.Add(connection);
			}

			return result;
		}
	}
}