using System.Collections.Generic;
using System.Net;
using ZTravel.API.VRRF;

namespace ZTravel.API.DBF {
	public class DbfApi {
		public static List<Departure> GetDepartures(string api, string station, string via = "") {
			var url =
				$"https://{api}/{WebUtility.UrlEncode(station)}?show_realtime=1&via={WebUtility.UrlEncode(via)}&mode=json&version=3";
			var raw  = new WebClient().DownloadString(url);
			var json = DbfResponse.FromJson(raw);
			return json.Departures;
		}
	}
}