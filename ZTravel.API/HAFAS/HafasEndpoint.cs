using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;

namespace ZTravel.API.HAFAS {
	public class HafasEndpoint {
		private readonly string _endpoint;
		private readonly string _client;
		private readonly string _version;
		private readonly string _auth;
		private readonly string _lang;

		public HafasEndpoint(string endpoint, string client, string version, string auth, string lang) {
			_endpoint = endpoint;
			_client   = client;
			_version  = version;
			_auth     = auth;
			_lang     = lang;
		}

		public List<HafasDeparture> GetDepartures(string station, int count) {
			var client = new WebClient();
			var data =
				$"{{\"ver\":\"{_version}\",\"lang\":\"{_lang}\",\"auth\":{_auth},\"client\":{_client},\"formatted\":false,\"ext\":\"VAO.11\",\"svcReqL\":[{{\"req\":{{\"stbLoc\":{{\"lid\":\"A=1@O={station}\"}},\"type\":\"DEP\",\"sort\":\"PT\",\"maxJny\":{count}}},\"meth\":\"StationBoard\"}}]}}";
			var resp   = client.UploadString(_endpoint, data);
			var parsed = HafasDepartureRawResponse.FromJson(resp);
			System.IO.File.WriteAllText("last_request.json", parsed.ToJson());
			var departures = new List<HafasDeparture>();
			var journeys   = parsed.SvcResL[0].Res.JnyL;
			var infos      = parsed.SvcResL[0].Res.Common.ProdL;

			foreach (var jny in parsed.SvcResL[0].Res.JnyL) {
				if (departures.Any(p => p.Jid == jny.Jid))
					continue;
				var      info    = infos[(Index) jny.ProdX];
				var      planDep = DateTime.ParseExact($"{jny.Date} {jny.StbStop.DTimeS}", "yyyyMMdd HHmmss", CultureInfo.InvariantCulture);
				DateTime realDep;
				try {
					realDep = DateTime.ParseExact($"{jny.Date} {jny.StbStop.DTimeR}", "yyyyMMdd HHmmss", CultureInfo.InvariantCulture);
				}
				catch {
					realDep = planDep;
				}

				var dep = new HafasDeparture(info.Number, jny.DirTxt, jny.StbStop.DPlatfS, planDep, realDep, jny.Jid);
				departures.Add(dep);
			}

			return departures;
		}
	}

	public class HafasDeparture {
		public readonly string   Line;
		public readonly string   Destination;
		public readonly string   Platform;
		public readonly DateTime PlannedDeparture;
		public readonly DateTime RealDeparture;

		public readonly string Jid;

		public HafasDeparture(string line, string destination, string platform, DateTime plannedDeparture, DateTime realDeparture, string jid) {
			Line             = line;
			Destination      = destination;
			Platform         = platform;
			PlannedDeparture = plannedDeparture;
			RealDeparture    = realDeparture;
			Jid              = jid;
		}
	}

	public class Endpoints {
		public static HafasEndpoint SvvEndpoint = new HafasEndpoint("https://fahrplan.salzburg-verkehr.at/bin/mgate.exe",
																	"{\"id\":\"VAO\",\"type\":\"WEB\",\"name\":\"webapp\",\"l\":\"vs_svv\"}", "1.20",
																	"{\"type\":\"AID\",\"aid\":\"wf7mcf9bv3nv8g5f\"}", "deu");
	}
}