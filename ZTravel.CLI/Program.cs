﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using ZTravel.API.VRRF;
using ZTravel.API.DBF;

namespace ZTravel.CLI {
	internal static class Departures {
		private static void Mainold(string[] args) {
			if (args[0] == "vrrf") {
				var deps = args.Length switch {
					6 => VrrfApi.GetDepartures(args[1], args[2], args[3], args[4], platform: args[5]).Raw,
					5 => VrrfApi.GetDepartures(args[1], args[2], args[3], args[4]).Raw,
					_ => new List<Raw>()
				};

				foreach (var dep in deps) {
					var rawtime      = DateTime.ParseExact($"{dep.SchedDate} {dep.Time}", "dd.MM.yyyy HH:mm", CultureInfo.InvariantCulture);
					var countdownRaw = rawtime - DateTime.Now;
					var countdown    = Math.Round(countdownRaw.TotalMinutes);
					var time         = countdown <= 60 ? $"{countdown,2} min" : $"{dep.Time,6}";
					if (countdown <= 0)
						time = "   now";
					if (countdown <= -1)
						continue;

					Console.WriteLine($"{dep.Line,-3} -> {dep.Destination.Replace($"{args[3]} ", ""),-30} {time}");
				}
			}
			
			if (args[0] == "dbf") {
				var deps = args.Length switch {
					3 => DbfApi.GetDepartures(args[1], args[2]),
					4 => DbfApi.GetDepartures(args[1], args[2], args[3]),
					_ => new List<Departure>()
				};

				foreach (var dep in deps.Where(p => p.ScheduledDeparture != null)) {
					Console.WriteLine($"{dep.Train,-10} -> {dep.Destination,-30} {dep.ScheduledDeparture}");
				}
			}
		}
	}
}