using System;
using System.Linq;
using ZTravel.API.HAFAS;

namespace ZTravel.CLI {
	public class TestMain {
		private static void Main(string[] args) {
			foreach (var dep in Endpoints.SvvEndpoint.GetDepartures("Salzburg Justizgebäude", 20).OrderBy(p => p.PlannedDeparture)) {
				if (int.Parse(dep.Line) >= 20)
					continue;
				var delay = (dep.RealDeparture - dep.PlannedDeparture).TotalMinutes;
				Console.WriteLine($"{dep.Platform} {dep.PlannedDeparture:HH:mm} ({(delay > 0 ? "+" : "")}{delay}) {dep.Line} {dep.Destination}");
			}
		}
	}
}